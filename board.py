"""
board.py
"""

import copy
from gamestate import GameState

class Board(GameState):
    """
    Board game state - a set of positions, each of which can
    contain one object (or nothing).
    """

    def __init__(self, size):
        """
        Initializes an empty board with size squares,
        each containing None.
        """
        self._positions = size * [None]

    def copy(self):
        return copy.deepcopy(self)
    
    def valid_position(self, position):
        return position >= 0 and position < len(self._positions)

    def all_positions(self):
        return [position for position in range(len(self._positions))]

    def empty_positions(self):
        return [position for position in range(len(self._positions))
                if not self.has_object(position)]
                
    def has_object(self, position):
        assert self.valid_position(position)
        return self._positions[position] is not None

    def get_object(self, position):
        """
        Returns the object at position, or None if it is empty.
        """
        assert self.valid_position(position)
        return self._positions[position]
    
    def remove_object(self, position):
        """
        Removes the object at position from the board.
        """
        assert self.has_object(position)
        self._positions[position] = None
        
    def place_object(self, position, obj):
        """
        Places the object obj at position.
        Requires position is a valid, empty position on the board.
        """
        assert self.valid_position(position)
        assert not self.has_object(position)
        self._positions[position] = obj

    @staticmethod
    def obj_unparse(obj):
        if obj:
            return str(obj)
        else:
            return '-'

    def serialize(self):
        """
        Returns a unique string that represents this position.
        """
        return ''.join([self.obj_unparse(obj) for obj in self._positions])
                
    def __str__(self):
        return "[ " + ' '.join([self.obj_unparse(obj)
                               for obj in self._positions]) \
                + " ]"


def test_board():
    b = Board(5)
    assert len(b.empty_positions()) == 5
    assert b.get_object(3) == None
    b.place_object(3, "X")
    assert len(b.empty_positions()) == 4
    assert 3 not in b.empty_positions()
    assert b.get_object(3) == "X"
    b.place_object(2, "O")
    assert len(b.empty_positions()) == 3
    assert 2 not in b.empty_positions()
    assert b.get_object(2) == "O"
    print (str(b))
    b.remove_object(2)
    assert b.get_object(2) == None
    print (str(b))

