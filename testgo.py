"""
testgo.py

Provides code for testing a Go implementation.
"""

# This assumes you implement the GameState for go in
# a class named GoBoard defined in go.py.  If you
# do something else, you'll need to change this to
# use the file and class you defined (or change your
# names to match this one).

# You should definitely not rely on this code as your
# only way to test your implementation, but we hope
# this will be useful to check that you understand the
# rules of go (the way we think they are supposed to work)
# and that your implementation seems to follow them
# (at least for these tests).

from go import GoBoard

from squareboard import Position
from player import Player

def test_go():
    print("Testing go:")
    black = Player("Black", "X")
    white = Player("White", "O")

    print("Creating board...")
    state = GoBoard(3, [black, white])
    
    assert state.serialize() == "---------"
    print("Passed empty board!")
    
    assert(len(state.possible_moves(black)) == 9)
    assert(len(state.possible_moves(white)) == 9)
    print("Passed possible moves")
    
    state.make_move(black, Position(0, 0))
    print("Made move: " + str(Position(0, 0)))

    assert state.serialize() == "X--------"
    assert(len(state.possible_moves(black)) == 8)
    assert state.has_liberties(Position(0, 0))
    print("Passed first move...")
    
    state.make_move(white, Position(1, 0))
    
    assert state.serialize() == "XO-------"
    assert state.has_liberties(Position(0, 0))
    assert state.has_liberties(Position(1, 0))
    print("Passed second move...")
    
    state.make_move(white, Position(0, 1))
    # should remove "X" at 0, 0 because it has no liberties
    assert state.serialize() == "-O-O-----"
    assert not state.has_object(Position(0, 0))
    print("Passed third move [capture]...")

    state.make_move(black, Position(1, 1))
    assert state.serialize() == "-O-OX----"
    print("Passed fourth move...")

    state.make_move(black, Position(1, 2))
    assert state.serialize() == "-O-OX--X-"
    print("Passed fifth move...")

    state.make_move(white, Position(2, 1))
    assert state.serialize() == "-O-OXO-X-"
    print("Passed sixth move...")
    
    # does not capture before of shared liberties
    state.make_move(white, Position(0, 2))
    assert state.serialize() == "-O-OXOOX-"
    print("Passed seventh move...")

    # double capture!
    state.make_move(white, Position(2, 2))
    assert state.serialize() == "-O-O-OO-O"
    print("Passed eigth move [doube capture]...")

    state.make_move(black, Position(1, 2))
    assert state.serialize() == "-O-O-OOXO"
    print("Passed ninth move...")

    # not legal: would lead to repeated state (both X's would be removed)
    assert not state.legal_move(black, Position(1, 1))

    # not legal - suicide move, restores previous position
    assert not state.legal_move(black, Position(0, 0))

    print("Success - passed all tests!")

if __name__ == "__main__":
    test_go()
