"""
gamestate.py
"""

class GameState:
    """
    Generic GameState module. Actual games will need
    to provide a subclass of GameState that actually
    implements these methods.
    """
    def __init__(self):
        pass

    def make_move(self, player, move):
        raise RuntimeException("No generic update implemented!")
        
    def legal_move(self, player, move):
        raise RuntimeException("No generic legal_move implemented!")

    def game_over(self):
        raise RuntimeException("No generic game_over implemented!")

    def outcome(self):
        raise RuntimeException("No generic outcome implemented!")

    def serialize(self):
        
        raise RuntimeException("No generic serialize implemented!")
    
    def __str__(self):
        return "<Generic Game State>"
    
